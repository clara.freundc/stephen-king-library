// les modules
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// les composants
import { BookCardComponent } from '../components/book-card/book-card.component';
import { BookDetailsComponent } from '../components/book-details/book-details.component';
import { AppComponent } from './app.component';

export const routes: Routes = [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'home', component: AppComponent },
    { path: 'list', component: BookCardComponent},
    { path: 'details/:id', component: BookDetailsComponent },
];

// Décorateur NgModule indiqué par le '@' permet de déclarer le module 'AppRoutingModule'
@NgModule({
    // Permet d'utiliser les bonnes routes
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }