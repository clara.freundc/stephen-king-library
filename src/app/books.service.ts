import { Injectable } from '@angular/core';
import { Book } from '../interfaces/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  url =
  'https://openlibrary.org'
  //isbn/9780140328721.json'
  //OL7353617M

  constructor() { }

  async getAllBooks(): Promise<any> {
    const data = await fetch(this.url + '/authors/OL2162284A/works.json').then((response) => response.json());
    //console.log(data.entries);
    return data.entries;
  }

  async getBooksById(key: string): Promise<any> {
    const dataDetails = await fetch(this.url + key + ".json").then((responseDetails) => responseDetails.json());
    console.log(dataDetails);
    return dataDetails;
  }
}
