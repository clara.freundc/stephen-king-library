import { Component, inject } from '@angular/core';
import { BooksService } from '../../app/books.service';
import { Book } from '../../interfaces/book';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-book-card',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './book-card.component.html',
  styleUrl: './book-card.component.css'
})
export class BookCardComponent { 
  //bookService = inject(BooksService);
  Books:any
  //Array<Book>

  constructor(private bookService: BooksService) {
    this.bookService.getAllBooks().then((r) => (this.Books = r)).then(() => console.log(this.Books));
  }
}
