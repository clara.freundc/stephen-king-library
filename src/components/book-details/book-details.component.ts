import { Component } from '@angular/core';
import { Book } from '../../interfaces/book';
import { BooksService } from '../../app/books.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-details',
  standalone: true,
  imports: [],
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.css'
})
export class BookDetailsComponent {
  currentBook:any;
  bookId= "";

  constructor(private bookService: BooksService, private activeRoute: ActivatedRoute) {
    this.bookId = String(this.activeRoute.snapshot.params['id'])
    this.bookService.getBooksById(this.bookId).then((r) => (this.currentBook = r)).then(() => console.log(this.currentBook));
  }
}
