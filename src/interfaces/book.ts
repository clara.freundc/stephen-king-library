export interface Book {
    authors: Array<any>;
    covers: Array<any>;
    created: object;
    description: object;
    key: string;
    last_modified: object;
    latest_revision: number;
    revision: number;
    subject_people: Array<any>;
    subject_places: Array<any>;
    subject_times: Array<any>;
    subjects: Array<any>;
    title: string;
    type: object;
}